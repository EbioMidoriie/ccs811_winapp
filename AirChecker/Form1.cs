﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Timers;
using System.Xml.Linq;

namespace AirChecker
{
    public partial class Form1 : Form
    {
        const string APP_VERSION = "Ver 0.11";
        static SerialPort sp;
        bool DebugMode;
        System.Timers.Timer Recivetimer;
        static XElement level_setting_data;
        String setting_file_path = @"Iaq_Level_Setting.xml";
        int[,] IAQ_Levels_Value;

        public Form1(params string[] arg)
        {
            InitializeComponent();
            this.Text = "空気質確認" + " " + APP_VERSION;

            // List ComPort and Select 1st item
            pmListCom();
            if (comboBox_port.Items.Count >= 1)
            {
                comboBox_port.SelectedIndex = 0;
            }

            // IAQ Levelのロード
            XDocument xml = XDocument.Load(setting_file_path);
            level_setting_data = xml.Element("CCS811");
            IAQ_Levels_Value = new int[2, 5];
            IAQ_Levels_Value[0, 0] = Convert.ToInt32(level_setting_data.Element("CO2_Level").Element("LV0").Value);
            IAQ_Levels_Value[0, 1] = Convert.ToInt32(level_setting_data.Element("CO2_Level").Element("LV1").Value);
            IAQ_Levels_Value[0, 2] = Convert.ToInt32(level_setting_data.Element("CO2_Level").Element("LV2").Value);
            IAQ_Levels_Value[0, 3] = Convert.ToInt32(level_setting_data.Element("CO2_Level").Element("LV3").Value);
            IAQ_Levels_Value[0, 4] = Convert.ToInt32(level_setting_data.Element("CO2_Level").Element("LV4").Value);

            IAQ_Levels_Value[1, 0] = Convert.ToInt32(level_setting_data.Element("TVOC_Level").Element("LV0").Value);
            IAQ_Levels_Value[1, 1] = Convert.ToInt32(level_setting_data.Element("TVOC_Level").Element("LV1").Value);
            IAQ_Levels_Value[1, 2] = Convert.ToInt32(level_setting_data.Element("TVOC_Level").Element("LV2").Value);
            IAQ_Levels_Value[1, 3] = Convert.ToInt32(level_setting_data.Element("TVOC_Level").Element("LV3").Value);
            IAQ_Levels_Value[1, 4] = Convert.ToInt32(level_setting_data.Element("TVOC_Level").Element("LV4").Value);


            // デバッグモードの時は、CH0の取得値ラベルを表示する
            DebugMode = false;
            try
            {
                Recivetimer = new System.Timers.Timer();
                Recivetimer.Elapsed += new ElapsedEventHandler(OnReciveTimer);
                Recivetimer.Interval = 500;

                if (arg[0] == "debug")
                {
                    SetDebugModeOn();
                }
            }
            catch { }
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (bt_port_start.Text == "開始")
            {
                try
                {
                    sp = new SerialPort();
                    sp.PortName = comboBox_port.SelectedItem.ToString();
                    sp.BaudRate = 115200;
                    sp.DataBits = 8;
                    sp.Parity = Parity.None;
                    sp.StopBits = StopBits.One;
                    sp.Encoding = Encoding.ASCII;

                    sp.Open();
                    bt_port_start.Text = "停止";
                    timGetData.Start();
                }
                catch
                {
                    sp.Close();
                    sp.Dispose();
                    timGetData.Stop();
                    bt_port_start.Text = "開始";
                }
            }
            else
            {
                timGetData.Stop();
                sp.Close();
                sp.Dispose();
                bt_port_start.Text = "開始";
            }
        }
        // PRIVATE
        private void pmListCom()
        {
            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                this.comboBox_port.Items.Add(port);
            }
        }

        private void SetDebugModeOn()
        {
            DebugMode = true;
            groupBox_debug.Visible = true;
        }

        private void timGetData_Tick(object sender, EventArgs e)
        {
            try
            {
                // シリアルデータを取得
                string spdata = sp.ReadExisting();
                int[] gas_data = SplitRawdata(spdata);

                // 生データラベルの更新
                if (gas_data[0] != 0)
                {
                    // for Debug - 受信ラベルを光らせる
                    if (DebugMode == true)
                    {
                        label_recive.Visible = true;
                        label_recive.Update();
                        Recivetimer.Start();
                    }

                    // 現在値の更新
                    label_eCO2.Text = gas_data[0].ToString();
                    label_eCO2.Update();

                    label_TVOC.Text = gas_data[1].ToString();
                    label_TVOC.Update();

                    // check IAQ
                    int IAQLevel = EvaGas(gas_data);

                    moveLocation(IAQLevel);
                }
            }
            catch
            {
                // 何かしらの理由によって、シリアルポートが切断された場合
                timGetData.Stop();
                sp.Close();
                sp.Dispose();
                MessageBox.Show("シリアルポートが切断されました。\n再接続してください");
                bt_port_start.Text = "開始";
            }


        }
        private int EvaGas(int[] gas_raw)
        {
            int CO2Level = 0;

            // CO2
            if (gas_raw[0] > IAQ_Levels_Value[0, 4])
            {
                CO2Level = 5;
            }
            else if (gas_raw[0] > IAQ_Levels_Value[0, 3])
            {
                CO2Level = 4;
            }
            else if (gas_raw[0] > IAQ_Levels_Value[0, 2])
            {
                CO2Level = 3;
            }
            else if (gas_raw[0] > IAQ_Levels_Value[0, 1])
            {
                CO2Level = 2;
            }
            else if (gas_raw[0] > IAQ_Levels_Value[0, 0])
            {
                CO2Level = 1;
            }else
            {
                CO2Level = 0;
            }

            // TVOC
            int TVOCLevel = 0;
            if (gas_raw[1] > IAQ_Levels_Value[1, 4])
            {
                TVOCLevel = 5;
            }
            else if (gas_raw[1] > IAQ_Levels_Value[1, 3])
            {
                TVOCLevel = 4;
            }
            else if (gas_raw[1] > IAQ_Levels_Value[1, 2])
            {
                TVOCLevel = 3;
            }
            else if (gas_raw[1] > IAQ_Levels_Value[1, 1])
            {
                TVOCLevel = 2;
            }
            else if (gas_raw[1] > IAQ_Levels_Value[1, 0])
            {
                TVOCLevel = 1;
            }
            else
            {
                TVOCLevel = 0;
            }

            int IAQLevel = 0;
            if (CO2Level > TVOCLevel)
            {
                IAQLevel = CO2Level;
            }else
            {
                IAQLevel = TVOCLevel;
            }


            return IAQLevel;
        }

        private void moveLocation(int IAQLevel)
        {
            switch (IAQLevel)
            {
                case 0:
                    pictureBox_cur.Animate("Location", new Point(346, 63)).Execution();
                    break;
                case 1:
                    pictureBox_cur.Animate("Location", new Point(346, 104)).Execution();
                    break;
                case 2:
                    pictureBox_cur.Animate("Location", new Point(346, 137)).Execution();
                    break;
                case 3:
                    pictureBox_cur.Animate("Location", new Point(346, 176)).Execution();
                    break;
                case 4:
                    pictureBox_cur.Animate("Location", new Point(346, 218)).Execution();
                    break;
                case 5:
                    pictureBox_cur.Animate("Location", new Point(346, 255)).Execution();
                    break;
                default:
                    pictureBox_cur.Animate("Location", new Point(346, 104)).Execution();
                    break;
            }


            // 以下はカーソルアニメーションのテスト
            /*
            int[] cur_pos = { 0, 0 };

            if (pictureBox_cur.Location.Y == 63)
            {
                pictureBox_cur.Animate("Location", new Point(346, 104)).Execution();
            }
            else if (pictureBox_cur.Location.Y == 104)
            {
                pictureBox_cur.Animate("Location", new Point(346, 137)).Execution();
            }
            else if (pictureBox_cur.Location.Y == 137)
            {
                pictureBox_cur.Animate("Location", new Point(346, 176)).Execution();
            }
            else if (pictureBox_cur.Location.Y == 176)
            {
                pictureBox_cur.Animate("Location", new Point(346, 218)).Execution();
            }
            else if (pictureBox_cur.Location.Y == 218)
            {
                pictureBox_cur.Animate("Location", new Point(346, 255)).Execution();
            }
            else if (pictureBox_cur.Location.Y == 255)
            {
                pictureBox_cur.Animate("Location", new Point(346, 63)).Execution();
            }
            else
            {
                pictureBox_cur.Animate("Location", new Point(346, 63)).Execution();
            }
            */

        }
        private int[] SplitRawdata(string str)
        {
            string rawdata_eCO2 = "";
            string rawdata_TVOC = "";
            int[] Gas = new int[2];

            if (str.IndexOf("Receive Data(") >= 0)
            {
                str = str.Substring(str.IndexOf("Receive Data("));
                try
                {
                    rawdata_eCO2 = str.Substring(13, 4);
                    rawdata_TVOC = str.Substring(18, 4);
                }
                catch
                {
                    rawdata_eCO2 = "0";
                    rawdata_TVOC = "0";
                }

                try
                {
                    Gas[0] = Convert.ToInt32(rawdata_eCO2,16);
                    Gas[1] = Convert.ToInt32(rawdata_TVOC, 16);
                }
                catch
                {
                    Gas[0] = 0;
                    Gas[1] = 0;
                }
            }
            return Gas;
        }
        private void OnReciveTimer(object sender, ElapsedEventArgs e)
        {
            label_recive.Visible = false;
            label_recive.Update();
            Recivetimer.Stop();
        }
    }
}
