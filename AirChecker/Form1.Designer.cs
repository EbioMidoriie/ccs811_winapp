﻿namespace AirChecker
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timGetData = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bt_port_start = new System.Windows.Forms.Button();
            this.comboBox_port = new System.Windows.Forms.ComboBox();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.groupBox_debug = new System.Windows.Forms.GroupBox();
            this.label_recive = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox_cur = new System.Windows.Forms.PictureBox();
            this.pictureBox_back = new System.Windows.Forms.PictureBox();
            this.groupBox_ValueLabel = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label_TVOC = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_eCO2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox_debug.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_cur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_back)).BeginInit();
            this.groupBox_ValueLabel.SuspendLayout();
            this.SuspendLayout();
            // 
            // timGetData
            // 
            this.timGetData.Interval = 1000;
            this.timGetData.Tick += new System.EventHandler(this.timGetData_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.bt_port_start);
            this.groupBox1.Controls.Add(this.comboBox_port);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(608, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "スタート";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "ポート";
            // 
            // bt_port_start
            // 
            this.bt_port_start.Location = new System.Drawing.Point(163, 46);
            this.bt_port_start.Name = "bt_port_start";
            this.bt_port_start.Size = new System.Drawing.Size(121, 36);
            this.bt_port_start.TabIndex = 1;
            this.bt_port_start.Text = "開始";
            this.bt_port_start.UseVisualStyleBackColor = true;
            this.bt_port_start.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox_port
            // 
            this.comboBox_port.FormattingEnabled = true;
            this.comboBox_port.Location = new System.Drawing.Point(163, 19);
            this.comboBox_port.Name = "comboBox_port";
            this.comboBox_port.Size = new System.Drawing.Size(121, 20);
            this.comboBox_port.TabIndex = 0;
            // 
            // serialPort
            // 
            this.serialPort.BaudRate = 115200;
            // 
            // groupBox_debug
            // 
            this.groupBox_debug.Controls.Add(this.label_recive);
            this.groupBox_debug.Location = new System.Drawing.Point(13, 455);
            this.groupBox_debug.Name = "groupBox_debug";
            this.groupBox_debug.Size = new System.Drawing.Size(595, 39);
            this.groupBox_debug.TabIndex = 1;
            this.groupBox_debug.TabStop = false;
            this.groupBox_debug.Text = "groupBox2";
            this.groupBox_debug.Visible = false;
            // 
            // label_recive
            // 
            this.label_recive.AutoSize = true;
            this.label_recive.Font = new System.Drawing.Font("メイリオ", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_recive.ForeColor = System.Drawing.Color.Red;
            this.label_recive.Location = new System.Drawing.Point(65, 15);
            this.label_recive.Name = "label_recive";
            this.label_recive.Size = new System.Drawing.Size(42, 24);
            this.label_recive.TabIndex = 0;
            this.label_recive.Text = "受信";
            this.label_recive.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pictureBox_cur);
            this.groupBox2.Controls.Add(this.pictureBox_back);
            this.groupBox2.Location = new System.Drawing.Point(13, 119);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(401, 330);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "計測結果";
            // 
            // pictureBox_cur
            // 
            this.pictureBox_cur.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_cur.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_cur.Image")));
            this.pictureBox_cur.Location = new System.Drawing.Point(346, 255);
            this.pictureBox_cur.Name = "pictureBox_cur";
            this.pictureBox_cur.Size = new System.Drawing.Size(48, 36);
            this.pictureBox_cur.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_cur.TabIndex = 1;
            this.pictureBox_cur.TabStop = false;
            // 
            // pictureBox_back
            // 
            this.pictureBox_back.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_back.Image")));
            this.pictureBox_back.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox_back.InitialImage")));
            this.pictureBox_back.Location = new System.Drawing.Point(7, 40);
            this.pictureBox_back.Name = "pictureBox_back";
            this.pictureBox_back.Size = new System.Drawing.Size(333, 274);
            this.pictureBox_back.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_back.TabIndex = 0;
            this.pictureBox_back.TabStop = false;
            // 
            // groupBox_ValueLabel
            // 
            this.groupBox_ValueLabel.Controls.Add(this.label5);
            this.groupBox_ValueLabel.Controls.Add(this.label_TVOC);
            this.groupBox_ValueLabel.Controls.Add(this.label4);
            this.groupBox_ValueLabel.Controls.Add(this.label3);
            this.groupBox_ValueLabel.Controls.Add(this.label_eCO2);
            this.groupBox_ValueLabel.Controls.Add(this.label2);
            this.groupBox_ValueLabel.Location = new System.Drawing.Point(421, 120);
            this.groupBox_ValueLabel.Name = "groupBox_ValueLabel";
            this.groupBox_ValueLabel.Size = new System.Drawing.Size(200, 329);
            this.groupBox_ValueLabel.TabIndex = 3;
            this.groupBox_ValueLabel.TabStop = false;
            this.groupBox_ValueLabel.Text = "計測値";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(148, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "ppb";
            // 
            // label_TVOC
            // 
            this.label_TVOC.AutoSize = true;
            this.label_TVOC.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_TVOC.Location = new System.Drawing.Point(43, 219);
            this.label_TVOC.Name = "label_TVOC";
            this.label_TVOC.Size = new System.Drawing.Size(36, 37);
            this.label_TVOC.TabIndex = 4;
            this.label_TVOC.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "現在のTVOC濃度";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(148, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "ppm";
            // 
            // label_eCO2
            // 
            this.label_eCO2.AutoSize = true;
            this.label_eCO2.Font = new System.Drawing.Font("MS UI Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_eCO2.Location = new System.Drawing.Point(43, 79);
            this.label_eCO2.Name = "label_eCO2";
            this.label_eCO2.Size = new System.Drawing.Size(36, 37);
            this.label_eCO2.TabIndex = 1;
            this.label_eCO2.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "現在のeCO2濃度";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 506);
            this.Controls.Add(this.groupBox_ValueLabel);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox_debug);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox_debug.ResumeLayout(false);
            this.groupBox_debug.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_cur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_back)).EndInit();
            this.groupBox_ValueLabel.ResumeLayout(false);
            this.groupBox_ValueLabel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timGetData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bt_port_start;
        private System.Windows.Forms.ComboBox comboBox_port;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.GroupBox groupBox_debug;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox_ValueLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_TVOC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_eCO2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox_back;
        private System.Windows.Forms.PictureBox pictureBox_cur;
        private System.Windows.Forms.Label label_recive;
    }
}

